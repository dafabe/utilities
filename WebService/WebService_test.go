package WebService_test

import (
	"Utilities/WebService"
	"testing"
)

func Test(t *testing.T) {

	tests := map[string]struct {
		url      string
		dbInfo   WebService.Databaser
		scanner  WebService.Scanner

		fetchUrl string
		want     string
	}{
		"one": {
			url: "/myurl",
			dbInfo: &WebService.StubDbInfo{},
			scanner: &WebService.People{},
			fetchUrl: "http://localhost:3050/myurl",
			want: `[{"name":"Bob","age":"35"},{"name":"TIM","age":"2"},{"name":"JAKE","age":"25"}]`,
		},
		"two": {
			url: "/myurl1",
			dbInfo: &WebService.DogDbInfo0{},
			scanner: &WebService.Dogs{},
			fetchUrl: "http://localhost:3050/myurl1?owner=ted",
			want: `[{"name":"rex","age":"1","owner":"ted"},{"name":"jet","age":"2","owner":"ted"},{"name":"max","age":"3","owner":"ted"}]`,
		},
		"three": {
			url: "/myurl2",
			dbInfo: &WebService.DogDbInfo2{},
			scanner: &WebService.Dogs{},
			fetchUrl: "http://localhost:3050/myurl2?owner=ted&name=rex",
			want: `[{"name":"rex","age":"1","owner":"ted"}]`,
		},

	}

	for _, tc := range tests {
		webService := WebService.NewWebService(tc.url, tc.dbInfo, tc.scanner)
		go webService.Start()
	}

	go WebService.Listen(3050)
	for name, tc := range tests {

		t.Run(name, func(t *testing.T) {
			got := WebService.Get(tc.fetchUrl)

			if got != tc.want {
				t.Errorf("got: %v, want: %v", got, tc.want)
			}
		})
	}
}

func TestPost(t *testing.T) {


	tests := map[string]struct {
		url      string
		dbInfo   WebService.Databaser
		scanner  WebService.Scanner
		fetchUrl string
		want     string
	}{
		"fourvq": {
			url: "/login",
			dbInfo: &WebService.UsernamePassword{},
			scanner: &WebService.Users{},
			fetchUrl: "http://localhost:3056/login?username=admin&password=admin",
			want: `<Response><success>true</success></Response>`,
		},
		"fivevq": {
			url: "/login_false",
			dbInfo: &WebService.UsernamePasswordNill{},
			scanner: &WebService.Users{},
			fetchUrl: "http://localhost:3056/login_false?username=admin&password=xxxxx",
			want: `<Response><success>false</success></Response>`,
		},

	}

	for _, tc := range tests {
		webService := WebService.NewPostWebService(tc.url, tc.dbInfo, tc.scanner)
		go webService.Start()
	}

	go WebService.Listen(3056)
	for name, tc := range tests {

		t.Run(name, func(t *testing.T) {
			got := WebService.Get(tc.fetchUrl)

			if got != tc.want {
				t.Errorf("got: %v, want: %v", got, tc.want)
			}
		})
	}
}

