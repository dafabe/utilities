package WebService

import (
	"database/sql"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

type WebServicer interface {
	Start()
}

type Scanner interface {
	Scan(rows *sql.Rows)
}

type WebService struct {
	url      string
	database Databaser
	scanner  Scanner
}

func NewWebService(url string, database Databaser, scanner Scanner) *WebService {
	return &WebService{url: url, database: database, scanner: scanner}
}
func (ws *WebService) Start() {

	go http.HandleFunc(ws.url, ws.action())

	// go Listen(port)

}

func Listen(port int) {
	portStr := ":" + strconv.Itoa(port)
	if err := http.ListenAndServe(portStr, nil); err != nil {
		log.Fatal(err)
	}
}
func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func (ws *WebService) action() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		dbInfo := ws.database

		db := dbInfo.GetDB()
		defer db.Close()

		query := dbInfo.Query()

		//------------------------------------------------------------------------------
		iparams := iparams(r)
		//------------------------------------------------------------------------------

		rows, err := db.Query(query, iparams...)

		defer rows.Close()
		ws.scanner.Scan(rows)

		if err != nil {
			log.Fatal(err)
		}

		err = json.NewEncoder(w).Encode(ws.scanner)

		if err != nil {
			fmt.Println(err)
		}
	}
}


func iparams(r *http.Request) []interface{} {
	var iparams []interface{}
	params := Params(r)
	for _, param := range params {
		iparams = append(iparams, param)
	}
	return iparams
}

type PostWebService struct {
	url      string
	database Databaser
	scanner  Scanner
}

func NewPostWebService(url string, database Databaser, scanner Scanner) *PostWebService {
	return &PostWebService{url: url, database: database, scanner: scanner}
}



type Response struct {
	Success bool `xml:"success"`
}

func NewResponse(success bool) *Response {
	return &Response{Success: success}
}
func (ws *PostWebService) Start() {

	go http.HandleFunc(ws.url, ws.action())

	// go Listen(port)

}
func (ws *PostWebService) action() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		enableCors(&w)
		dbInfo := ws.database

		db := dbInfo.GetDB()
		defer db.Close()

		query := dbInfo.Query()

		//------------------------------------------------------------------------------
		iparams := iparams(r)
		//------------------------------------------------------------------------------

		rows, err := db.Query(query, iparams...)

		if err != nil {
			fmt.Println(err)
		}
		defer rows.Close()

		next := rows.Next()
		if next {
			v := NewResponse(true)
			xml.NewEncoder(w).Encode(v)
		} else {
			v := NewResponse(false)
			err = xml.NewEncoder(w).Encode(v)
			if err != nil {
				fmt.Println(err)
			}
		}


	}
}
